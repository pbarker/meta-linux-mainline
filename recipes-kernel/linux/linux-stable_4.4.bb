# Copyright (C) 2021, meta-linux-mainline contributors (auto-generated file)
# SPDX-License-Identifier: CC0-1.0
LINUX_VMAJOR = "4"
LINUX_VMINOR = "4"
LINUX_VPATCH = "299"
LINUX_SHA256_TARBALL = "401d7c8fef594999a460d10c72c5a94e9c2e1022f16795ec51746b0d165418b2"
LINUX_SHA256_PATCH = "e58e4d560498daa1084e792baa35b33ab12356c988c7fbd39e6d413140ffe6ea"
require linux-stable.inc
