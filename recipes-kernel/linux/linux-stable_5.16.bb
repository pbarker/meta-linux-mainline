# Copyright (C) 2021, meta-linux-mainline contributors (auto-generated file)
# SPDX-License-Identifier: CC0-1.0
LINUX_VMAJOR = "5"
LINUX_VMINOR = "16"
LINUX_VPATCH = "2"
LINUX_SHA256_TARBALL = "027d7e8988bb69ac12ee92406c3be1fe13f990b1ca2249e226225cd1573308bb"
LINUX_SHA256_PATCH = "3a09c2f1ad410c09cf03921abeed1a6ca7c38138fb508171ee673d429d179171"
require linux-stable.inc
