# Copyright (C) 2021, meta-linux-mainline contributors (auto-generated file)
# SPDX-License-Identifier: CC0-1.0
LINUX_VMAJOR = "5"
LINUX_VMINOR = "10"
LINUX_VPATCH = "93"
LINUX_SHA256_TARBALL = "dcdf99e43e98330d925016985bfbc7b83c66d367b714b2de0cbbfcbf83d8ca43"
LINUX_SHA256_PATCH = "04b1fcebcc2eb5ecfe79520859c602b4f0a879919bdd4d15b34e2d892fc86c36"
require linux-stable.inc
