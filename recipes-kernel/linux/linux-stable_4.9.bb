# Copyright (C) 2021, meta-linux-mainline contributors (auto-generated file)
# SPDX-License-Identifier: CC0-1.0
LINUX_VMAJOR = "4"
LINUX_VMINOR = "9"
LINUX_VPATCH = "297"
LINUX_SHA256_TARBALL = "029098dcffab74875e086ae970e3828456838da6e0ba22ce3f64ef764f3d7f1a"
LINUX_SHA256_PATCH = "aec085899297cee90b015abab14917486124b30a82786ad5069daab470b4f05e"
require linux-stable.inc
