# Copyright (C) 2021, meta-linux-mainline contributors (auto-generated file)
# SPDX-License-Identifier: CC0-1.0
LINUX_VERSION = "5.17-rc1"
SRCREV = "e783362eb54cd99b2cac8b3a9aeac942e6f6ac07"
require linux-mainline.inc
