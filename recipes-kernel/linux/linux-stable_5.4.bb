# Copyright (C) 2021, meta-linux-mainline contributors (auto-generated file)
# SPDX-License-Identifier: CC0-1.0
LINUX_VMAJOR = "5"
LINUX_VMINOR = "4"
LINUX_VPATCH = "173"
LINUX_SHA256_TARBALL = "bf338980b1670bca287f9994b7441c2361907635879169c64ae78364efc5f491"
LINUX_SHA256_PATCH = "00db8c0e33f631ea9a50a99f1e08ab79258dd819258c98beace9e154835eb49f"
require linux-stable.inc
